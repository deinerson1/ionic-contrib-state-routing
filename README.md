# ionic-contrib-state-routing

A public Ionic seed that has navigational view/state security.
When a view/state requires permission, the user is redirected to the Login
view/state, but can still navigate to public views/states.

### What is this repository for?

* To unify ui.router state security with angularfire-seed
* Version 0.1.0

### How do I get set up?

~~~~
npm i -g cordova ionic gulp bower
cd ionic-contrib-state-routing
npm install
bower install
ionic serve

cordova save plugins --experimental
ionic platform add android
ionic build android
ionic emulate android
~~~~

### Configuration

* Change config.js FBURL

`.constant('FBURL', 'https://FireBaseURL.firebaseio.com');`

### Dependencies

* angular 1.2.26
* firebase 1.0.24
* firebase-simple-login 1.6.4
* angularfire 0.8.2
* angular-animate 1.2.26
* angular-sanitize 1.2.26
* angular-ui-router 0.2.11


### Who do I talk to?

* Dave, repo owner

***

[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
