/* Services */

(function (angular) {
  "use strict";

  angular.module('starter.services', [])

    .service('messageList', ['fbutil', function (fbutil) {
        return fbutil.syncArray('messages', {limitToLast : 10, endAt : null});
      }]);
    
})(angular);

