
/* Filters */

(function () {
  "use strict";
  (function (angular) {

    angular.module('starter.filters', [])
      .filter('interpolate', ['version', function (version) {
          return function (text) {
            return String(text).replace(/\%VERSION\%/mg, version);
          };
        }])

      .filter('reverse', function () {
          return function (items) {
            return items.slice().reverse();
          };
        });
  })(angular);
})();