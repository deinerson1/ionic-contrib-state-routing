// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
(function (angular) {
  "use strict";
  angular.module('starter', [
    'ionic',
    'firebase',
    'starter.config',
    'starter.controllers',
    'starter.decorators',
    'starter.directives',
    'starter.filters',
    'starter.routes',
    'starter.services',
    'ui.router'
  ])

    .run(['$ionicPlatform', 'simpleLogin', '$rootScope', '$state', '$log',
      function ($ionicPlatform, simpleLogin, $rootScope, $state, $log) {
        $ionicPlatform.ready(function () {
          // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
          // for form inputs)
          if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          }
          if (window.StatusBar) {
            StatusBar.styleDefault();
          }

          $log.debug('ionic-contrib-state-routing starter app running.');

          $rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
            if (angular.isObject(error) && error.authRequired) {
              if (event) {
                event.preventDefault();
              }
              $state.go('login');
            }
            if (error === 'AUTH_REQUIRED') {

            if (event) {
              event.preventDefault();
            }
              $state.go('login');
            }
          });

        });
      }]);

})(angular);