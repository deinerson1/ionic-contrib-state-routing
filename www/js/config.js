(function (angular) {
  "use strict";

  // Declare app level module which depends on filters, and services
  angular.module('starter.config', [])

    // version of this seed app is compatible with angularFire 0.8.2
    // see tags for other versions: https://github.com/firebase/angularFire-seed/tags
    .constant('version', '0.1.1')

    // where to redirect users if they need to authenticate; see stateSecurity.js
    .constant('loginStateName', 'login')
    .constant('alreadyLoggedInStateName', 'account')

    // A Firebase data URL goes here, no trailing slash
    .constant('FBURL', 'https://unm-test.firebaseio.com');
})(angular);