/* Controllers */

(function (angular) {
  "use strict";

  angular.module('starter.controllers', [])
    .controller('HomeCtrl', ['$scope', 'fbutil', 'user', 'FBURL', '$log',
      function ($scope, fbutil, user, FBURL, $log) {
        $log.debug('HomeCtrl fired.');

        $scope.syncedValue = fbutil.syncObject('syncedValue');
        $scope.auth = user;
        $scope.FBURL = FBURL;
      }])

    .controller('ChatCtrl', ['$scope', 'messages', '$log', 'user',
      function ($scope, messages, $log, user) {
        $log.debug('ChatCtrl fired.');

        $scope.messages = messages;
        $scope.addMessage = function (newMessage) {
          if (newMessage) {
            $scope.messages.$add({
              text : newMessage,
              created : Firebase.ServerValue.TIMESTAMP,
              uid : user.uid
            });
          }
        };
      }])

    .controller('LoginCtrl', ['$scope', 'simpleLogin', '$state', '$log', 'user',
      function ($scope, simpleLogin, $state, $log, user) {
        $log.debug('LoginCtrl fired.');

        $scope.email = null;
        $scope.pass = null;
        $scope.confirm = null;
        $scope.createMode = false;
        $scope.createModeToggle = function() {
          $scope.createMode = !$scope.createMode;
        };

        $scope.login = function (email, pass) {
          $scope.err = null;
          simpleLogin.login(email, pass)
            .then(function (/* user */) {
              //$scope.user= user;
              $state.go('account');
            }, function (err) {
              $scope.err = errMessage(err);
            });
        };

        $scope.createAccount = function (email, pass, confirm, createMode) {
          $scope.err = null;
          if (assertValidAccountProps(email, pass, confirm, createMode)) {
            simpleLogin.createAccount(email, pass)
              .then(function (/* user */) {
                $state.go('account');
              }, function (err) {
                $scope.err = errMessage(err);
              });
          }
        };

        function assertValidAccountProps(email, pass, confirm, createMode) {
          if (!email) {
            $scope.err = 'Please enter an email address';
          }
          else if (!pass || !confirm) {
            $scope.err = 'Please enter a password';
          }
          else if (createMode && pass !== confirm) {
            $scope.err = 'Passwords do not match';
          }
          return !$scope.err;
        }

        function errMessage(err) {
          return angular.isObject(err) && err.code ? err.code : err + '';
        }
      }])

    .controller('AccountCtrl', ['$scope', 'simpleLogin', 'fbutil', 'user', '$state', '$log',
      function ($scope, simpleLogin, fbutil, user, $state, $log) {
        $log.debug('AccountCtrl fired.');

        $log.debug('user', user.uid);
        // create a 3-way binding with the user profile object in Firebase
        var profile = fbutil.syncObject(['users', user.uid]);
        profile.$bindTo($scope, 'profile');

        // expose logout function to scope
        $scope.logout = function () {
          profile.$destroy();
          simpleLogin.logout();
          $state.go('login');
        };

        $scope.changePassword = function (pass, confirm, newPass) {
          resetMessages();
          if (!pass || !confirm || !newPass) {
            $scope.err = 'Please fill in all password fields';
          }
          else if (newPass !== confirm) {
            $scope.err = 'New pass and confirm do not match';
          }
          else {
            simpleLogin.changePassword(profile.email, pass, newPass)
              .then(function () {
                $scope.msg = 'Password changed';
              }, function (err) {
                $scope.err = err;
              });
          }
        };

        $scope.clear = resetMessages;

        $scope.changeEmail = function (pass, newEmail) {
          resetMessages();
          profile.$destroy();
          simpleLogin.changeEmail(pass, newEmail)
            .then(function (user) {
              profile = fbutil.syncObject(['users', user.uid]);
              profile.$bindTo($scope, 'profile');
              $scope.emailmsg = 'Email changed';
            }, function (err) {
              $scope.emailerr = err;
            });
        };

        function resetMessages() {
          $scope.err = null;
          $scope.msg = null;
          $scope.emailerr = null;
          $scope.emailmsg = null;
        }
      }]);
})(angular);