/* Routes using $stateProvider of ui.router injected in app.js */

(function (angular) {
  "use strict";
  angular.module('starter.routes', [])

    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $stateProvider
          .state('home', {
            url : "/home",
            templateUrl : "templates/home.html",
            controller : 'HomeCtrl',
            data : {
              authRequired : false
            },
            resolve : {
              // forces the page to wait for this promise to resolve before controller is loaded
              // the controller can then inject `user` as a dependency. This could also be done
              // in the controller, but this makes things cleaner (controller doesn't need to worry
              // about auth status or timing of displaying its UI components)
              user : ['simpleLogin', function (simpleLogin) {
                  return simpleLogin.getUser();
                }]
            }
          })

          .state('chat', {
            url : '/chat',
            data : {
              authRequired : true
            },
            templateUrl : 'templates/chat.html',
            controller : 'ChatCtrl',
            controllerAs : 'messages',
            resolve : {
              messages : ['messageList', function (messageList) {
                  return messageList;
                }],
              user : ['simpleLogin', function (simpleLogin) {
                  return simpleLogin.requireAuth();
                }]
            }
          })

          .state('login', {
            url : '/login',
            data : {
              authRequired : false
            },
            templateUrl : 'templates/login.html',
            controller : 'LoginCtrl',
            resolve : {
              user : ['simpleLogin', function (simpleLogin) {
                  return simpleLogin.getUser();
                }]
            }
          })

          .state('account', {
            url : '/account',
            templateUrl : 'templates/account.html',
            controller : 'AccountCtrl',
            data : {
              authRequired : true
            },
            resolve : {
              user : ['simpleLogin', function (simpleLogin) {
                  return simpleLogin.requireAuth();
                }]
            }
          });

        $urlRouterProvider.otherwise('/home');
      }]);

})(angular);